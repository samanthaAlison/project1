/**
 * \file DrawableGameComponentVisitor.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class that describes a drawable game component visitor.
 */


#pragma once

class DrawableGameComponentChild;
class SpritedGameComponent;
class CPlayingCircle;
class CInventoryItem;
class CPokestop;


/**
 * Visitor class for drawable game components.
 */
class CDrawableGameComponentVisitor
{
public:
	// Constructor
	CDrawableGameComponentVisitor();
	// Destructor
	virtual ~CDrawableGameComponentVisitor();

	/// Visits the playing circle
	virtual void VisitPlayingCircle(CPlayingCircle * mCircle) {}
	/// Visits an inventory item
	virtual void VisitInventoryItem(CInventoryItem * item) {}
	/// Visits a pokestop
	virtual void VisitPokestop(CPokestop *pokestop) {}
};

