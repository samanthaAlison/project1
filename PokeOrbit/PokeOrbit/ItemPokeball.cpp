/**
 * \file ItemPokeball.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "ItemPokeball.h"


/// Image file of the inventory item
const std::wstring PokeballImageFile(L"images/pokeball.png");


/**
 * Constructor. 
 * \param parent Parent of this component (should be the Inventory class).  
 * \param x X coordinate of the component relative to parent.
 * \param y Y coordinate of the component relative to parent. 
 */
CItemPokeball::CItemPokeball(CDrawableGameComponent * parent, double x, double y) : CInventoryItem(parent, x, y, PokeballImageFile, CItemType::Pokeball)
{
}


/**
 * Destructor.
 */
CItemPokeball::~CItemPokeball()
{
}
