/**
 * \file SpritedGameComponent.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "SpritedGameComponent.h"

using namespace std;
using namespace Gdiplus;


/**
* Constructor. Assigns the parent of this component, gives it a position, and the filename of its sprite.
* \param parent Parent of this component.
* \param x X coordinate of the component.
* \param y Y coordinate of the component.
* \param filename Filename of the component's sprite
*/
CSpritedGameComponent::CSpritedGameComponent(CDrawableGameComponent * parent, double x, double y, const std::wstring &filename) : CDrawableGameComponentChild(parent, x, y)
{
	mSprite = unique_ptr<Bitmap>(Bitmap::FromFile(filename.c_str()));
	if (mSprite->GetLastStatus() != Ok)
	{
		wstring msg(L"Failed to open ");
		msg += filename;
		AfxMessageBox(msg.c_str());
	}
	else
	{
		SetWidth(mSprite->GetWidth());
		SetHeight(mSprite->GetHeight());
	}
}


/**
 * Destructor.
 */
CSpritedGameComponent::~CSpritedGameComponent()
{
}


/**
 * Draws the sprite of this component, and then all of it's children.
 * \param graphics Pointer to Graphics renderer.
 */
void CSpritedGameComponent::Draw(Gdiplus::Graphics *graphics)
{
	graphics->DrawImage(mSprite.get(),
		float(GetX() - GetWidth() / 2.0f), float(GetY() - GetHeight() / 2.0f),
		(float)CDrawableGameComponent::GetWidth() * (float)mScale, (float)CDrawableGameComponent::GetHeight() * (float)mScale );
	CDrawableGameComponentChild::Draw(graphics);

}


/**
*  Test to see if we hit this object with a mouse.
* \param x X position to test
* \param y Y position to test
* \return true if hit.
*/
bool CSpritedGameComponent::IsAtCoordinates(int x, int y)
{
	double wid = mSprite->GetWidth() / 2; // divided by 2 because the width and height are half the original
	double hit = mSprite->GetHeight() / 2;
	double xX = GetX();
	double yY = GetY();

	// Make x and y relative to the top-left corner of the bitmap image
	// Subtracting the center makes x, y relative to the center of the image.
	// Adding half the size makes x, y relative to the top corner of the image
	double testX = x - GetX() + wid / 2;
	double testY = y - GetY() + hit / 2;

	if (testX < 0 || testY < 0 || testX >= wid || testY >= hit)
	{
		// We are outside the image
		return false;
	}

	// Test to see if x, y are in the drawn part of the image
	auto format = mSprite->GetPixelFormat();
	if (format == PixelFormat32bppARGB || format == PixelFormat32bppPARGB)
	{
		// This image has an alpha map, which implements the 
		// transparency. If so, we should check to see if we
		// clicked on a pixel where alpha is not zero, meaning
		// the pixel shows on the screen.
		Color color;
		mSprite->GetPixel((int)testX, (int)testY, &color);
		return color.GetAlpha() != 0;
	}
	else {
		return true;
	}
}