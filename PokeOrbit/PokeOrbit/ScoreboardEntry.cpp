/**
 * \file ScoreboardEntry.cpp
 *
 * \author Samantha Oldenburg
 * \author Keerthana Kolisetty
 * \author Stefani Taskas
 */


#include "stdafx.h"
#include "ScoreboardEntry.h"
#include <sstream>


/// color for score board entry
const Gdiplus::Color White(Gdiplus::Color(255, 255, 255));

/// font size for the score entry
const float PointDisplayFontSize= 12;


/**
 * Constructor
 * \param parent The parent of this entry.
 * \param x The x location of this entry.
 * \param y The y location of this entry.
 * \param type The type of pokemon this entry is.
 */
CScoreboardEntry::CScoreboardEntry(CDrawableGameComponent *parent, double x, double y, CPokemonType::Type type) : CDrawableGameComponentChild(parent, x, y),  mPokemonSprite(CComponentFactory::CreateSpritedGameComponent(this, 0.0, 0.0, CPokemonType::GetFile(type)))
{
	mType = type;
	AddChild(mPokemonSprite);
}


/**
* Destructor
*/
CScoreboardEntry::~CScoreboardEntry()
{
}


/**
* Draws the scoreboard entry
* \param graphics the graphics needed to draw the scoreboard entry
*/
void CScoreboardEntry::Draw(Gdiplus::Graphics * graphics)
{
	Gdiplus::SolidBrush textColor(White);
	Gdiplus::FontFamily fontFamily(L"Arial");
	Gdiplus::Font font(&fontFamily, PointDisplayFontSize);

	std::wstringstream str;
	str << " x " << mCaught;

	Gdiplus::RectF textSize;
	graphics->MeasureString(str.str().c_str(), -1, &font, Gdiplus::PointF(0, 0), &textSize);
	graphics->DrawString(str.str().c_str(),  // String to draw
		-1,         // String length, -1 means it figures it out on its own
		&font,      // The font to use
		Gdiplus::PointF(float(GetX() + mPokemonSprite->GetWidth() / 2),
			float(GetY() + mPokemonSprite->GetHeight() / 2 - textSize.Height)),   // Where to draw (top left corner)
		&textColor);    // The brush to draw the text with

	CDrawableGameComponentChild::Draw(graphics);
}
