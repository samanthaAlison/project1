/**
 * \file Pikachu.h
 *
 * \author Keerthana Kolisetty
 * \author Stefani Taskas
 *
 * Derived class of pokemon for the pikachu pokemon
 */


#pragma once
#include "Pokemon.h"


 /**
 * Pikachu class whose base class is Pokemon
 */
class CPikachu :
	public CPokemon
{
public:
	CPikachu(const CPikachu &) = delete;
	CPikachu(CDrawableGameComponent * parent, double x, double y);
	virtual ~CPikachu();
};

