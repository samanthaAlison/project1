/**
 * \file DrawableGameComponentChild.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Describes a subset of the DrawableGameComponent class
 * that is a child of a parent component
 */


#pragma once
#include "DrawableGameComponent.h"


/**
 * DrawableGameComponent that has a parent component
 */
class CDrawableGameComponentChild :
	public CDrawableGameComponent
{
public:
	// Default constructor deleted.
	CDrawableGameComponentChild() = delete;
	// Copy constructor deleted.
	CDrawableGameComponentChild(const CDrawableGameComponentChild &) = delete;
	// Destructor
	virtual ~CDrawableGameComponentChild();
	
	// Constructor
	CDrawableGameComponentChild(CDrawableGameComponent * parent, double x, double y);

	// Gets the absolute X coordinates of this component.
	virtual double GetX() override;
	// Gets the absolute Y coordinates of this component.
	virtual double GetY() override;

	// Checks to see if a pokestop was clicked.
	virtual bool HitTest(int x, int y);

	/**
	* Used for the pokeballs, tells us if the game object has left the radius of the circle
	* \returns true if out of the circle, false otherwise
	*/
	virtual bool OutOfBounds() { return false; } // always false rn not finished

	/**
	* Test this game component to see if it is a pokemon
	* \returns Boolean. true if game component is a pokemon or false if it is not
	*/
	virtual bool Pokemon() { return false; }

	/**
	* Code called when a pokeball hits a pokemon. Adds a point
	* to the scoreboard.
	* \param pokemon Pointer to the pokemon object we caught.
	*/
	virtual void PokemonCaught(CPokemon *pokemon) { GetParent()->PokemonCaught(pokemon); };

	/**
	* Adds pokeballs to the inventory
	* \param count The amount of pokeballs to add.
	*/
	virtual void AddPokeballs(int count) { GetParent()->AddPokeballs(count); };

	/**
	* Removes the Pokemon when the ball catches it.
	* \returns boolean true or false depending on if the pokemon was removed.
	*/
	virtual bool Remove() { return GetParent()->RemoveChild(this); }

protected:
	/**
	 * Get the parent of this component
	 * \returns A pointer to the parent of this component
	 */
	CDrawableGameComponent * GetParent() { return mParent; }

private:
	/// Parent of this component.
	CDrawableGameComponent * mParent;
};

 