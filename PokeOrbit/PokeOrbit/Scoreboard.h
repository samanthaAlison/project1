/**
 * \file Scoreboard.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes a menu item that displays the pokemon the user has
 * caught.
 */

#pragma once
#include "DrawableGameComponentChild.h"
#include "ScoreboardEntry.h"
/**
 * Displays the pokemon the user has caught.
 */
class CScoreboard : public CDrawableGameComponentChild
{
public:
	CScoreboard(CDrawableGameComponent * parent, double x, double y);
	virtual ~CScoreboard();

	bool AddPoint(CPokemonType::Type pokemon);



private:
	void AddEntry(CPokemonType::Type pokemon);

	///the entries
	std::vector<std::shared_ptr<CScoreboardEntry> > mEntries;
};

