/**
* \file Charmander.cpp
*
* \author Keerthana Kolisetty
* \author Stefani Taskas
*/


#include "stdafx.h"
#include "Charmander.h"

#include <random>


/// Maximum speed of a Charmander in radians per second
const double CharmanderMaxSpeed(1.8);
/// Minimum speed of a Charmander in radians per second
const double CharmanderMinSpeed(1.0);


/**
* Constructor
* \param parent Parent of this charmander
* \param x The x coordinate to place this charmander at
* \param y The y coordinate to place thic charmander at
*/
CCharmander::CCharmander(CDrawableGameComponent * parent, double x, double y): 
	CPokemon(parent, x,y, CPokemonType::Charmander)
{
	/// Random device
	std::random_device rdChar;
	/// Random generator
	std::mt19937 rngChar(rdChar());
	/// Range for random doubles
	std::uniform_real_distribution<double> radChar(CharmanderMinSpeed, CharmanderMaxSpeed);

	SetRadius(GetRandomRadius());
	SetTheta(GetSpeed());
	SetThetaSpeed(radChar(rngChar));
	SetRadiusSpeed(0.0);
}


/**
* Destructor
*/
CCharmander::~CCharmander()
{
}
