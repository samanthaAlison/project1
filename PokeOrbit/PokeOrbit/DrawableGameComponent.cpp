/**
 * \file DrawableGameComponent.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 */


#include "stdafx.h"
#include "DrawableGameComponent.h"
#include "DrawableGameComponentChild.h"
#include <iterator>
#include <algorithm>
#include "Pokemon.h"
#include "ClickTest.h"


/**
 * Constructor. Sets location of item
 * \param x X coordinate of component
 * \param y Y coordinate of component
 */
 CDrawableGameComponent::CDrawableGameComponent(double x, double y)
{
	mX = x;
	mY = y;
}


/**
 * Destructor. Cleans up memory.
 */
CDrawableGameComponent::~CDrawableGameComponent()
{
}


/**
 * Adds a pointer to a drawable game component to this 
 * component's children
 * \param child drawable game component to add
 */
void CDrawableGameComponent::AddChild(std::shared_ptr<CDrawableGameComponentChild> child)
{
	mChildren.push_back(child);
}


/**
 * Transforms the parent's coordinates with this component's relative
 * coordinates in order to create absolute coordinates for this object
 * \param x X coordinate of parent in pixels
 * \param y Y coordinate of parent in pixels
 */
void CDrawableGameComponent::Transform(double &x, double &y)
{
	x += mX;
	y += mY;
}


/**
 * Drawing function only used by the root drawable game 
 * component (the GameClass). Calls the draw function of the game's 
 * immediate children
 * \param graphics Pointer to the Graphics object
 */
void CDrawableGameComponent::Draw(Gdiplus::Graphics *graphics)
{
	for (const auto& child : mChildren)
	{
		child->Draw(graphics);
		double y = child->GetY();
		double x = child->GetX();
	}

}


/**
 * Checks if the child is a nullptr or not.
 * \param child The child of this parent.
 * \returns boolean false if the child isn't a nullptr.
 */
bool IsNullptr(const std::shared_ptr<CDrawableGameComponentChild> child)
{
	if (child != nullptr)
	{
		return false;
	}
	return true;
}


/**
 * Updates this component's children
 * \param elapsed Time passes since last frame in seconds
 */
void CDrawableGameComponent::Update(double elapsed)
{
	std::vector<std::shared_ptr<CDrawableGameComponentChild>> toActuallyRemove;
	for (const auto& child : mChildren)
	{
		if (child != nullptr)
			child->Update(elapsed);
	}
	mChildren.erase(std::remove_if(mChildren.begin(), mChildren.end(), IsNullptr), mChildren.end());

}


/**
 * Removes a child from this component.
 * \param child Child to be removed.
 * \returns True if the child was removed.
 */
bool CDrawableGameComponent::RemoveChild(CDrawableGameComponentChild* child)
{
	for (std::vector<std::shared_ptr<CDrawableGameComponentChild>>::iterator iter = mChildren.begin(); iter != mChildren.end(); ++iter)
	{

		if ((*iter).get() == child)
		{
			(*iter) = nullptr;
			return true;
		}
	}
	return false;
}


/**
* See if the pokeball catches a pokemon. 
* \param child Pokeball that has been thrown
* \returns true if the pokeball is over a pokemon
*/
bool CDrawableGameComponent::Caught(CDrawableGameComponentChild* child)
{
	for (auto &other : mChildren)
	{
		// Do not compare to ourselves
		if (other.get() == child) {
			continue;
		}
		
		// If other is a pokemon and is under the pokeballs current coordinates, catch it
		if (other != nullptr && other->Pokemon() && other->IsAtCoordinates((int)child->GetX(), (int)child->GetY()))
		{
			other->Remove();
			IsNullptr(other);
			return true;
		}
	}
	// Pokeball never caught a pokemon
	return false;
}

