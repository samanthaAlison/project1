/**
 * \file InventoryItemInquirer.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Describes a visitor class that looks through the inventory for a 
 * specific type of item.
 */


#pragma once
#include "DrawableGameComponentVisitor.h"
#include "InventoryItem.h"


/**
 * Visitor class that looks for inventory items.
 */
class CInventoryItemInquirer : public CDrawableGameComponentVisitor
{
public:
	// Constructor
	CInventoryItemInquirer(CItemType::Type type);
	// Destructor
	virtual ~CInventoryItemInquirer();

	/**
	 * Checks if the inquirer has found at least one item of the type 
	 * it is assigned to look for. 
	 * \returns True if it has found at least one item. */
	bool HasFoundItem() { return (mFoundCount > 0); }

	/**
	 * Gets the number of items of the type the inquirer was assigned
	 * that the inquirer has found.
	 * \returns The amount of items the inquirer has found.*/
	int GetFoundCount() { return mFoundCount; }

	// Function to visit an inventory item to see what type it is.
	virtual void VisitInventoryItem(CInventoryItem * item);

private:
	/// The type of item to look for.
	CItemType::Type mItemType;

	/// How many of a the type of item it has found
	int mFoundCount = 0;
};

