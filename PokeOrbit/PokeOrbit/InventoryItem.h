/**
 * \file InventoryItem.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class that describes an entry in our inventory. Holds
 * a sprite of the item, and is a child of the inventory class
 */


#pragma once
#include "SpritedGameComponent.h"
#include <string>
#include "ItemType.h"


/**
 * An item in our inventory
 */
class CInventoryItem : public CSpritedGameComponent
{
public:
	// Copy constructor deleted
	CInventoryItem(const CInventoryItem &) = delete;
	
	// Constructor
	CInventoryItem(CDrawableGameComponent * parent, double x, double y, const std::wstring &filename, CItemType::Type item);
	// Destructor
	virtual ~CInventoryItem();

	/**
	 * Get the type of item that this item is. 
	 * \returns The type of this item. */
	CItemType::Type GetItemType() { return mItemType; }

	/** Accept a visitor
	* \param visitor The visitor we accept */
	virtual void Accept(CDrawableGameComponentVisitor * visitor) override { visitor->VisitInventoryItem(this); }

private:
	/// The type of this inventory item
	CItemType::Type mItemType;
};

