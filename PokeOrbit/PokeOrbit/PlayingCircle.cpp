/**
 * \file PlayingCircle.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * \author Keerthana Kolisetty
 */


#include "stdafx.h"
#include <cmath> 
#include "PlayingCircle.h"
#include "PokeballInstance.h"
#include "ComponentFactory.h"

using namespace Gdiplus;


/// Filename of the Ash sprite to be drawn in the center of the playing circle.
const std::wstring PlayingCircleSprite(L"images/ash.png");

/// Value of pi
const double Pi = 3.1415926535897;
/// Radius of the circle.
const double Radius = 350.0;
/// color used to draw the circle.
const Color Green(0, 128, 0);
/// Thickness of circle border
const float CircleThickness(3.0f);


/**
* Constructor. Assigns the parent of this component, and gives it a position on the game space.
* \param parent Parent of this component.
* \param x X coordinate of the component.
* \param y Y coordinate of the component.
 */
CPlayingCircle::CPlayingCircle(CDrawableGameComponent * parent, double x, double y) : CSpritedGameComponent(parent, x, y, PlayingCircleSprite)
{
	mRadius = Radius;
}


/**
 *Initializes games with an emitter for the player circle
 */
void CPlayingCircle::Initialize()
{
	mEmitter = CComponentFactory::CreateCEmitter(this);
}


/**
 * Destructor.
 */
CPlayingCircle::~CPlayingCircle()
{
	delete mEmitter;
}


/**
 * Sets the direction of the thrown pokeball.
 * \param x The x location of the mouse click
 * \param y The y location of the mouse click
 */
void CPlayingCircle::ThrowPokeball(double x, double y)
{
	double theta = atan2(y, x);

	double radiusSpeed = sqrt( (x * x) + (y * y) );
	AddChild(std::make_shared<CPokeballInstance>(this, theta, radiusSpeed));
	
}


/**
 * Draws the playing circle, the Ash sprite, and all the circle's children.
 * \param graphics Pointer to graphics renderer
 */
void CPlayingCircle::Draw(Gdiplus::Graphics *graphics)
{
	Pen pen(Green, CircleThickness);
	graphics->DrawEllipse(&pen,(float)(GetX() - mRadius), (float)(GetY() - mRadius), (float)(2.0 * mRadius), (float)(2*mRadius));
	
	CSpritedGameComponent::Draw(graphics);
	CDrawableGameComponentChild::Draw(graphics);
}


/**
 * Make calls to update the emitter and sprited game components.
 * \param elapsed Time passed since last frame.
 */
void CPlayingCircle::Update(double elapsed)
{
	mEmitter->Update(elapsed);
	CSpritedGameComponent::Update(elapsed);
}
