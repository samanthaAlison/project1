/**
 * \file ComponentFactory.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class that describes a factory used to by drawable game components to 
 * create their children.
 */


#pragma once
#include <memory>
#include <string>
class CDrawableGameComponent;
class CDrawableGameComponentChild;
class CSpritedGameComponent;
class CPlayingCircle;
class CPokestop;
class CPokemon;
class CPikachu;
class CCharmander;
class CBulbasaur;
class CItemPokeball;
class CEmitter;


/**
 * Factory used to create drawable game components.
 */
class CComponentFactory
{
public:
	// Component factory constructor
	CComponentFactory();
	// Component factory destructor
	virtual ~CComponentFactory();

	// Creates the playing circle
	std::shared_ptr<CPlayingCircle> CreatePlayingCircle(CDrawableGameComponent * parent, double x, double y);

	// Creates a pokestop
	static std::shared_ptr<CPokestop> CreatePokestop(CDrawableGameComponent * parent, double x, double y);

	// Creates a pokeball item
	static std::shared_ptr<CItemPokeball>  CreateItemPokeball(CDrawableGameComponent * parent, double x, double y);

	// Creates a pikachu
	static std::shared_ptr<CPikachu>  CreatePikachu(CDrawableGameComponent * parent, double x, double y);
	// Creates a bulbasaur
	static std::shared_ptr<CBulbasaur> CreateBulbasaur(CDrawableGameComponent * parent, double x, double y);
	// Creates a charmander
	static std::shared_ptr<CCharmander> CreateCharmander(CDrawableGameComponent * parent, double x, double y);

	// Creates a sprited game component
	static std::shared_ptr<CSpritedGameComponent> CreateSpritedGameComponent(CDrawableGameComponent * parent, double x, double y, const std::wstring &filename);
	
	// Creates an emitter
	static CEmitter * CreateCEmitter(CPlayingCircle *circle);
};


