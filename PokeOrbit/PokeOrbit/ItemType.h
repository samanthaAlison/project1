/**
 * \file ItemType.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class used to identify different inventory items in the game.
 */


#pragma once


/**
 * Used to identify different inventory items
 */
class CItemType
{
public:
	/// Types of items that can be in the inventory
	enum Type { Pokeball };
};