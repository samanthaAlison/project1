/**
 * \file Scoreboard.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * \author Keerthana Kolisetty
 */


#include "stdafx.h"
#include "Scoreboard.h"


/// X length of scoreboard buffer
const double ScoreboardEntryYBufferLength(10.0);

/// Y length of scoreboard buffer
const double ScoreboardEntryXBufferLength(10.0);


/**
* Constructor
* \param parent The parent of the scoreboard
* \param x The x location of the scoreboard
* \param y The y location of the scoreboard
*/
CScoreboard::CScoreboard(CDrawableGameComponent * parent, double x, double y) : CDrawableGameComponentChild(parent, x, y)
{
}


/**
* Destructor
*/
CScoreboard::~CScoreboard()
{
}


/**
* Adds points based on pokemon
* \param pokemon The type of pokemon
* \returns bool true if a point was added, false otherwise
*/
bool CScoreboard::AddPoint(CPokemonType::Type pokemon)
{
	for (std::vector<std::shared_ptr<CScoreboardEntry >>::iterator iter = mEntries.begin(); iter != mEntries.end(); ++iter )
	{
		if ((*iter)->GetType() == pokemon)
		{
			(*iter)->AddPoint();
			return true;
		}
	}
	AddEntry(pokemon);
	AddChild(mEntries.back());
	mEntries.back()->AddPoint();
	double y = mEntries.back()->GetY();
	return false;
}


/**
* Adds entry to the scoreboard
* \param pokemon The type of pokemon
*/
void CScoreboard::AddEntry(CPokemonType::Type pokemon)
{
	double y = ScoreboardEntryYBufferLength;
	for (auto entry : mEntries)
	{
		y += ScoreboardEntryYBufferLength + entry->GetHeight();
	}
	mEntries.push_back(std::make_shared<CScoreboardEntry>(this, ScoreboardEntryXBufferLength, y, pokemon));
}