/**
 * \file DrawableGameComponent.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class that describes a drawable game component. Everything
 * drawn in this game is derived from this class. Contains children
 * drawable game components that are drawn relative to it. Only one object is 
 * directly derived from this class. 
 */


#pragma once
#include <vector>
#include <memory>
#include "DrawableGameComponentVisitor.h"
class CPokemon;
class CDrawableGameComponentChild;


/**
 * Base class for all drawable components of this game.
 */
class CDrawableGameComponent
{
public:
	// Default constructor deleted
	CDrawableGameComponent() = delete;
	// Copy constructor deleted
	CDrawableGameComponent(const CDrawableGameComponent &) = delete;
	// Drawable game component destructor
	virtual ~CDrawableGameComponent();
	
	// Adds a pointer to a drawable game component to this
	// component's children
	virtual void AddChild(std::shared_ptr<CDrawableGameComponentChild> child);
	
	/**
	* Gets the x coordinate of the component
	* \returns The x coordinate of this component
	*/
	virtual double GetY() { return mY; }
	
	/**
	* Gets the y coordinate of the component
	* \returns The y coordinate of this component
	*/
	virtual double GetX() { return mX; }

	// Updates this component's children
	virtual void Update(double elapsed);
	
	/**
	* Sets the x coordinate of the component
	* \param x The x coordinate to set this component to
	*/
	void SetX(double x) { mX = x; }
	
	/**
	* Sets the y coordinate of the component
	* \param y The y coordinate to set this component to
	*/
	void SetY(double y) { mY = y; }

	/**
	* Gets the height of the component
	* \returns The height of this component
	*/
	virtual double GetHeight() { return mHeight; }

	/**
	* Gets the width of the component
	* \returns The width of this component 
	*/
	virtual double GetWidth() { return mWidth; }

	// Removes a child from this component.
	bool RemoveChild(CDrawableGameComponentChild* child);

	// Test to see if the pokeball went over a pokemon
	bool Caught(CDrawableGameComponentChild * child);

	/**
	*Checks to see if a DrawableGameComponent is at this location
	* \param x position x
	* \param y position y
	* \returns false
	*/
	virtual bool IsAtCoordinates(int x, int y) { return false; }
	
	/** Accept a visitor
	* \param visitor The visitor we accept */
	virtual void Accept(CDrawableGameComponentVisitor *visitor) {}

	/** See if the pokeball catches a pokemon.
	* \param pokemon The pokemon being checked */
	virtual void PokemonCaught(CPokemon *pokemon) {};

	/** Add pokeballs to the inventory.
	* \param count The amount of pokeballs to add */
	virtual void AddPokeballs(int count) {};

protected: 
	// Constructor
	CDrawableGameComponent(double x, double y);
	
	/** Gets an iterator to the beginning of the children vector
	* \returns iterator to the beginning of the children vector */
	std::vector<std::shared_ptr<CDrawableGameComponentChild> >::iterator ChildrenBegin() { return mChildren.begin(); }
	/** Gets an iterator to the end of the children vector
	* \returns iterator to the end of the children vector */
	std::vector<std::shared_ptr<CDrawableGameComponentChild> >::iterator ChildrenEnd() { return mChildren.end(); }

	/** Gets a reverse iterator to the beginning of the children vector
	* \returns a reverse iterator to the beginning of the children vector */
	std::vector<std::shared_ptr<CDrawableGameComponentChild> >::reverse_iterator ChildrenRbegin() { return mChildren.rbegin(); }
	/** Gets a reverse iterator to the end of the children vector
	* \returns a reverse iterator to the end of the children vector */
	std::vector<std::shared_ptr<CDrawableGameComponentChild> >::reverse_iterator ChildrenRend() { return mChildren.rend(); }


	/** 
	 * Determines if this component has any children
	 * \returns true if the component has no children
	 */
	bool IsChildless() { return mChildren.empty(); }

	/**
	 * Sets the width of the component
	 * \param width The width to set this component to
	 */
	void SetWidth(double width) { mWidth = width; }
	
	/**
	* Sets the height of the component
	* \param height The width to set this component to
	*/
	void SetHeight(double height) { mHeight = height; }


	virtual void Draw(Gdiplus::Graphics *graphics);
	void Transform(double &x, double &y);

private:
	/// Children components of this component 
	std::vector<std::shared_ptr<CDrawableGameComponentChild> > mChildren;
	
	/// X location of component (relative to parent if component child) 
	double mX;
	/// Y location of component (relative to parent if component child) 
	double mY;
	/// Width of object
	double mWidth;
	/// Height of object
	double mHeight;

	/// Vector of pointers to children that need to be deleted.
	std::vector<std::shared_ptr<CDrawableGameComponentChild> > mToDelete;
};

