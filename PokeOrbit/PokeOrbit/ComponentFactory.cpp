/** 
 * \file ComponentFactory.cpp
 *
 * \author Samantha Oldenburg
 * \author Keerthana Kolisetty
 * \author Stefani Taskas
 */


#include "stdafx.h"
#include "ComponentFactory.h"
#include "PlayingCircle.h"
#include "Pokestop.h"
#include "Pokemon.h"

#include "Bulbasaur.h"
#include "Charmander.h"
#include "Pikachu.h"

#include "ItemPokeball.h"
#include "Emitter.h"


/**
 * Constructor.
 */
CComponentFactory::CComponentFactory()
{
}


/**
 * Destructor.
 */
CComponentFactory::~CComponentFactory()
{
}


/**
 * Constructs a playing circle and returns a pointer to it
 * \param parent Parent of the circle
 * \param x X coordinate of the circle
 * \param y Y coordinate of the circle
 * \returns A pointer to the constructed playing circle
 */
std::shared_ptr<CPlayingCircle>  CComponentFactory::CreatePlayingCircle(CDrawableGameComponent * parent, double x, double y)
{
	return std::make_shared<CPlayingCircle>(parent, x, y);
}
 

/**
* Constructs a pokestop and returns a pointer to it
* \param parent Parent of the pokestop
* \param x X coordinate of the pokestop
* \param y Y coordinate of the pokestop
* \returns A pointer to the constructed pokestop
*/
std::shared_ptr<CPokestop> CComponentFactory::CreatePokestop(CDrawableGameComponent * parent, double x, double y)
{
	return std::make_shared<CPokestop>(parent, x, y);
}


/**
* Constructs a pokeball inventory item and returns a pointer to it
* \param parent Parent of the item
* \param x X coordinate of the item
* \param y Y coordinate of the item
* \returns A pointer to the constructed pokeball item
*/
std::shared_ptr<CItemPokeball> CComponentFactory::CreateItemPokeball(CDrawableGameComponent * parent, double x, double y)
{
	return std::make_shared<CItemPokeball>(parent, x, y);
}


 /**
 * Constructs a pikachu and returns a pointer to it
 * \param parent Parent of the pikachu
 * \param x X coordinate of the pikachu
 * \param y Y coordinate of the pikachu
 * \returns A pointer to the constructed pikachu
 */
std::shared_ptr<CPikachu> CComponentFactory::CreatePikachu(CDrawableGameComponent * parent, double x, double y)
{
	return std::make_shared<CPikachu>(parent, x, y);
}


/**
* Constructs a bulbasaur and returns a pointer to it
* \param parent Parent of the bulbasaur
* \param x X coordinate of the bulbasaur
* \param y Y coordinate of the bulbasaur
* \returns A pointer to the constructed bulbasaur
*/
std::shared_ptr<CBulbasaur> CComponentFactory::CreateBulbasaur(CDrawableGameComponent * parent, double x, double y)
{
	return std::make_shared<CBulbasaur>(parent, x, y);
}


/**
* Constructs a charmander and returns a pointer to it
* \param parent Parent of the charmander
* \param x X coordinate of the charmander
* \param y Y coordinate of the charmander
* \returns A pointer to the constructed charmander
*/
std::shared_ptr<CCharmander>  CComponentFactory::CreateCharmander(CDrawableGameComponent * parent, double x, double y)
{
	return std::make_shared<CCharmander>(parent, x, y);
}


/**
* Constructs a sprited game component and returns a pointer to it
* \param parent Parent of the sprited game component
* \param x X coordinate of the sprited game component
* \param y Y coordinate of the sprited game component
* \param filename Name of the sprite's image file. 
*\returns A pointer to the constructed sprited game component
*/
std::shared_ptr<CSpritedGameComponent> CComponentFactory::CreateSpritedGameComponent(CDrawableGameComponent * parent, double x, double y, const std::wstring & filename)
{
	return std::make_shared<CSpritedGameComponent>(parent, x, y, filename);
}


/**
 * Constructs an emitter and returns a pointer to it
 * \param circle PlayingCircle that owns this emitter
 * \returns A pointer to the constructed emitter
 */
CEmitter * CComponentFactory::CreateCEmitter(CPlayingCircle *circle)
{
	return new CEmitter(circle);
}
