/**
 * \file ChildView.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * \author Keerthana Kolisetty
 */


// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "PokeOrbit.h"
#include "ChildView.h"
#include "PokeballInstance.h"
#include "PokeOrbitGame.h"
#include "Pokestop.h"

#include "DoubleBufferDC.h"
using namespace Gdiplus;
using namespace std;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/// Frame duration in milliseconds
const int FrameDuration = 30;

using namespace std;
using namespace Gdiplus;
// CChildView


/**
 * Constructor
 */
CChildView::CChildView()
{
}


/**
* Destructor
*/
CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()



// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}


/** This function is called to draw in the window.
*
* This function is called in response to a drawing message
* whenever we need to redraw the window on the screen.
* It is responsible for painting the window.
*/
void CChildView::OnPaint() 
{ 
	if (mFirstDraw)
	{
		CRect rc;
		GetClientRect(&rc);
		mGame = CPokeOrbitGame((double)rc.Width(), (double)rc.Height());
		mGame.Initialize();
		mFirstDraw = false;
		SetTimer(1, FrameDuration, nullptr);


		/*
		* Initialize the elapsed time system
		*/
		LARGE_INTEGER time, freq;
		QueryPerformanceCounter(&time);
		QueryPerformanceFrequency(&freq);

		mLastTime = time.QuadPart;
		mTimeFreq = double(freq.QuadPart);
	}

	/*
	* Compute the elapsed time since the last draw
	*/
	LARGE_INTEGER time;
	QueryPerformanceCounter(&time);
	long long diff = time.QuadPart - mLastTime;
	double elapsed = double(diff) / mTimeFreq;
	mLastTime = time.QuadPart;
	
	mGame.Update(elapsed);

	CPaintDC paintDC(this);     // device context for painting
	CDoubleBufferDC dc(&paintDC); // device context for painting
	Graphics graphics(dc.m_hDC);	// Create GDI+ graphics context.

	graphics.Clear(Color(0, 0, 0));

	CRect rect;
	GetClientRect(&rect);
	// TODO: Add your message handler code here
	mGame.DrawFrame(&graphics, rect.Width(), rect.Height());
	// Do not call CWnd::OnPaint() for painting messages
}


/**
 * Throws a pokeball or collects pokeballs from a pokestop when the mouse is clicked
 * within the radius of the circle.
 * \param nFlags Flags associated with the mouse movement
 * \param point Where the button was pressed 
 */
void CChildView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// Check if click was inside the circle
	if (mGame.ClickTest(point.x, point.y))
	{
		// Then if it was, HitTest to see if a pokestop was clicked on
	
		if (mGame.HitTest(point.x, point.y))
		{
			// pokestop was clicked, add pokeballs to the inventory
			//mGame.AddPokeballs(3);
			Invalidate();
		}
		else 
		{
			// A pokestop was not clicked, throw a pokeball
			mGame.ThrowPokeball(point.x, point.y);
			Invalidate();
		}
	}
}


/**
* Handle timer events
* \param nIDEvent The timer event ID
*/
void CChildView::OnTimer(UINT_PTR nIDEvent)
{
	Invalidate();
	CWnd::OnTimer(nIDEvent);
}

/**
* Erase the background
*
* This is disabled to eliminate flicker
* \param pDC Device context
* \returns FALSE
*/
BOOL CChildView::OnEraseBkgnd(CDC* pDC)
{
	return FALSE;
}
