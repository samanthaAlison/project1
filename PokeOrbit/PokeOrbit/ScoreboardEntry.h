/**
* \file ScoreboardEntry.h
*
* \author Samantha Oldenburg
* \author Keerthana Kolisetty
* Class that describes each of the menu items in the scoreboard
*/
#pragma once
#include "DrawableGameComponentChild.h"
#include "SpritedGameComponent.h"
#include "PokemonType.h"
#include "ComponentFactory.h"
/**
* Displays the pokemon the user has caught.
*/
class CScoreboardEntry : public CDrawableGameComponentChild
{
public:
	CScoreboardEntry(CDrawableGameComponent *parent, double x, double y, CPokemonType::Type type);
	virtual ~CScoreboardEntry();

	/**
	* type of  pokemon
	* \returns the type of pokemon
	*/
	CPokemonType::Type GetType() { return mType; }


	/**
	* type of  pokemon
	* \returns the type of pokemon
	*/
	virtual double GetHeight() override { return mPokemonSprite->GetHeight(); }

	void Draw(Gdiplus::Graphics *graphics) override; 

	/**
	 *Adds points
	 */
	void AddPoint() { mCaught++; }
private:
	/// The spirited Sprite
	std::shared_ptr<CSpritedGameComponent> mPokemonSprite;

	/// The number caught
	int mCaught = 0;

	/// The type of pokemon
	CPokemonType::Type mType;

};

