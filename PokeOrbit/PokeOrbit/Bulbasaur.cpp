/**
* \file Bulbasaur.cpp
*
* \author Keerthana Kolisetty
* \author Stefani Taskas
*/


#include "stdafx.h"
#include "Bulbasaur.h"

#include <random>


/// Maximum speed of a bulbasaur in radians per second
const double BulbasaurMaxSpeed(0.8);
/// Minimum speed of a bulbasaur in radians per second
const double BulbasaurMinSpeed(0.5);


/**
* Constructor
* \param parent Parent of this bulbasaur
* \param x The x coordinate to place this bulbasaur at
* \param y The y coordinate to place thic bulbasaur at
*/
CBulbasaur::CBulbasaur(CDrawableGameComponent * parent, double x, double y):
	CPokemon(parent,x,y, CPokemonType::Bulbasaur)
{
	/// Random device for bulbasaurs
	std::random_device rdBulb;
	/// Random generator for bulbasaurs
	std::mt19937 rngBulb(rdBulb());
	/// Range for random doubles
	std::uniform_real_distribution<double> radBulb(BulbasaurMinSpeed, BulbasaurMaxSpeed);

	SetRadius(GetRandomRadius());
	SetTheta(GetSpeed());
	SetThetaSpeed(radBulb(rngBulb));
	SetRadiusSpeed(0.0);
}


/**
* Destructor
*/
CBulbasaur::~CBulbasaur()
{
}
