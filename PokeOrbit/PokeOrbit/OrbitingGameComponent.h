/**
 * \file OrbitingGameComponent.h
 *
 * \author Samantha Oldenburg
 * \author Keerthana Kolisetty

 * Class that describes a type of DrawableGameComponent that orbits around 
 * the inside of the playing circle.
 */



#pragma once
#include "SpritedGameComponent.h"

/**
 * A type of DrawableGameComponent that orbits around the inside of the playing circle.
 */
class COrbitingGameComponent : public CSpritedGameComponent
{
public:
	COrbitingGameComponent(const COrbitingGameComponent &) = delete;

	COrbitingGameComponent(CDrawableGameComponent * parent, double x, double y, const std::wstring &filename);
	virtual ~COrbitingGameComponent();

protected: 
	virtual void Update(double elapsed);
	
	/**
	* Sets the angular velocity of the component.
	* \param thetaSpeed Angular velocity to set the speed to in radians per second
	*/
	void SetThetaSpeed(double thetaSpeed) { mThetaSpeed = thetaSpeed; }

	/**
	* Sets the rate at which object moves away from the circle center 
	* \param radiusSpeed Rate to set the speed to in pixels per second
	*/
	void SetRadiusSpeed(double radiusSpeed) { mRadiusSpeed = radiusSpeed; }
	
	/** 
	* Sets the theta of the component.
	* \param theta Angle to set theta to in radians
	*/
	void SetTheta(double theta) { mTheta= theta; }
	
	/**
	* Sets the radius of the component.
	* \param radius Magnitude to set radius to in pixels
	*/
	void SetRadius(double radius) { mRadius = radius; }

	/**
	* Gets the radius of the component.
	* \returns double The radius of this component
	*/
	double GetRadius() { return mRadius; }

	int GetRandomRadius();

	double GetSpeed();

private:
	void TransformToCartesian();


	/// distance from center of circle parent in pixels 
	double mRadius; 

	/// Angle the a line from the object to the center of the circle parent
	/// makes with a line made from the center of the circle to its
	/// right most edge, in radians.
	double mTheta; 

	/// Rate at which object moves around circle in radians per second
	double mThetaSpeed; 	
	
	/// Rate at which object moves away from the circle center in 
	/// pixels per second
	double mRadiusSpeed;
};

