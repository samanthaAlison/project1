/**
* \file Pokemon.cpp
*
* \author Keerthana Kolisetty
*/


#include "stdafx.h"
#include "Pokemon.h"
#include <random>


/**
* Constructor. Creates a pokemon
* \param parent Parent of this component.
* \param x X coordinate of the component.
* \param y Y coordinate of the component.
* \param type The type of the pokemon
*/
CPokemon::CPokemon(CDrawableGameComponent * parent, double x, double y,CPokemonType::Type type): COrbitingGameComponent(parent, x, y, CPokemonType::GetFile(type))
{
	mType = type;
	SetRadius((double) GetRandomRadius());
	SetThetaSpeed(GetSpeed());
	double r = GetRadius();
	double s = 0;
}


/**
* Destructor
*/
CPokemon::~CPokemon()
{
}


/**
* Removes the Pokemon when the ball is caught.
* \returns boolean
*/
bool CPokemon::Remove()
{
	GetParent()->PokemonCaught(this);
	return CDrawableGameComponentChild::Remove();
}
