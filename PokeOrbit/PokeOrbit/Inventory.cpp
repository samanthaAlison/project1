/**
 * \file Inventory.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 */


#include "stdafx.h"
#include "Inventory.h"


/// The Y buffer space between each item 
const double InventoryItemYBufferLength(10.0);

/// The X buffer space between the items and the left side of the inventory
const double InventoryItemXBufferLength(10.0);


/**
 * Constructor. 
 * \param parent Parent to assign componenet to 
 * \param x X coordinate of the component relative to parent
 * \param y Y coordinate of the component relative to parent
 */
CInventory::CInventory(CDrawableGameComponent * parent, double x, double y) : CDrawableGameComponentChild(parent, x, y)
{
}


/**
 * Destructor.
 */
CInventory::~CInventory()
{
}


/**
 * Adds inventory items to the inventory.
 * \param itemType The type of item to add.
 * \param count How many of the item to add.
 */
void CInventory::AddItem(CItemType::Type itemType, int count)
{
	for (int i = 0; i < count; i++)
	{
		// Only add more pokeballs if the inventory has less than 13 in the inventory already
		if(GetItemCount(itemType) < 13)
		{
			std::shared_ptr<CInventoryItem>  item;
			switch (itemType)
			{
			case (CItemType::Pokeball):
				item = CComponentFactory::CreateItemPokeball(this, InventoryItemXBufferLength, 0.0);
				break;
			}

			double menuYPlacement = InventoryItemYBufferLength;

			for (std::vector< std::shared_ptr<CDrawableGameComponentChild> >::iterator iter = ChildrenBegin(); iter != ChildrenEnd(); ++iter)
			{
				menuYPlacement += (*iter)->GetHeight() + InventoryItemYBufferLength;
			}

			item->SetY(menuYPlacement);
			AddChild(item);
		}
	}
}


/**
 * Removes items of a certain type from the inventory. 
 * \param item The type of item to remove
 * \param count How many of the items to remove
 * \returns True if the removal was successful
 */
bool CInventory::RemoveItem(CItemType::Type item, int count)
{
	if (count == 0)
	{
		return true;
	}
	else
	{
		CInventoryItemInquirer remover(item);
		for (std::vector<std::shared_ptr<CDrawableGameComponentChild >>::reverse_iterator remove_iter = ChildrenRbegin(); !remover.HasFoundItem() && remove_iter != ChildrenRend(); ++remove_iter)
		{
			double childHeight = (*remove_iter)->GetHeight();
			(*remove_iter)->Accept(&remover);
			if (remover.HasFoundItem())
			{
				for (std::vector<std::shared_ptr<CDrawableGameComponentChild> >::reverse_iterator transform_iter = ChildrenRbegin(); transform_iter != remove_iter; ++transform_iter)
				{
					(*transform_iter)->SetY((*transform_iter)->GetY() - InventoryItemYBufferLength - (*remove_iter)->GetWidth());
				}
				RemoveChild((*remove_iter).get());
				return RemoveItem(item, --count);
			}
		}
	}
	
	return false;
}


/**
 * Counts the amount of a certain item this inventory has.
 * \param item The type of item to count.
 * \returns The amount of the specified type of item in the inventory. 
 */
 int CInventory::GetItemCount(CItemType::Type item)
 {
	 CInventoryItemInquirer inquirer(item);
	 for (std::vector<std::shared_ptr<CDrawableGameComponentChild> >::iterator child = ChildrenBegin(); child != ChildrenEnd(); ++child)
	 {
		 (*child)->Accept(&inquirer);
	 }
	 return inquirer.GetFoundCount();
 }

