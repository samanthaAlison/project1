/**
* \file Charmander.h
*
* \author Keerthana Kolisetty
* \author Stefani Taskas
*
* Derived class of pokemon for the charmander pokemon
*/


#pragma once
#include "Pokemon.h"


/**
* Charmander class whose base class is Pokemon
*/
class CCharmander :
	public CPokemon
{
public:
	// The charmander constructor
	CCharmander(CDrawableGameComponent * parent, double x, double y);
	// The charmander destructor
	virtual ~CCharmander();
};

