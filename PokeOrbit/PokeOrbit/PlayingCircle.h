/**
 * \file PlayingCircle.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * \author Keerthana Kolisetty
 *
 * Class that describes a DrawableGameComponent that holds the playing cirlce
 * and all the interactable components of the game.
 */

#pragma once
#include "SpritedGameComponent.h"
#include "Emitter.h"
/**
 * The playing circle of the game.
 */
class CPlayingCircle : public CSpritedGameComponent
{
public:
	CPlayingCircle() = delete;
	CPlayingCircle(const CPlayingCircle &) = delete;
	CPlayingCircle(CDrawableGameComponent * parent, double x, double y);
	virtual ~CPlayingCircle();
	void Initialize();
	void ThrowPokeball(double x, double y);

	/**
	 * Gets the radius of the circle this object draws
	 * \returns The circle's radius in pixels */
	double GetRadius() { return mRadius; }
protected:
	virtual void Draw(Gdiplus::Graphics * graphics) override;
	virtual void Update(double elapsed) override;
private:
	/// Radius of the playing circle.
	double mRadius;

	/// The emitter that spawns objects in the circle
	CEmitter *mEmitter;
};

