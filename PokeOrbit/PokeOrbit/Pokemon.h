/**
* \file Pokemon.h
*
* \author Keerthana Kolisetty
*
* Pokemon class to make pokemon
*/


#pragma once
#include "OrbitingGameComponent.h"
#include "PokemonType.h"


/**
* Pokemon class whose base class is COrbitingGameComponent
*/
class CPokemon :
	public COrbitingGameComponent
{
public:
	/// Default constructor (disabled)
	CPokemon(const CPokemon &) = delete;

	CPokemon(CDrawableGameComponent * parent, double x, double y, CPokemonType::Type type);
	virtual ~CPokemon();

	/**
	* Returns the type of the pokemon
	* \returns Type
	*/
	CPokemonType::Type GetType() { return mType; }

	virtual bool Remove() override;
	/**
	* Test this game component to see if it is a pokemon
	* \returns Boolean. true if game component is a pokemon or false if it is not
	*/
	bool Pokemon() { return true; }

private:
	///Type of the pokemon
	CPokemonType::Type mType;
	
};

