/**
 * \file ChildView.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * 
 * interface of the CChildView class
 */


#pragma once
#include "PokeOrbitGame.h"
#include "DrawableGameComponent.h"

// CChildView window


/**
 * Childview class
 */
class CChildView : public CWnd
{
// Construction
public:
	CChildView();

// Attributes
public:

// Operations
public:

// Overrides
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
// Implementation
public:
	virtual ~CChildView();

	// Generated message map functions
protected:
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
private:
	/// The game that contains all of the pokestops, pokemon, pokeballs, etc.
	CPokeOrbitGame mGame;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

private:
	/// True until we draw the frame the first time.
	bool mFirstDraw = true;

	long long mLastTime;    ///< Last time we read the timer
	double mTimeFreq;       ///< Rate the timer updates

	/// The component clicked on, if a drawable component was clicked
	CDrawableGameComponent * mClickedItem;

public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};

