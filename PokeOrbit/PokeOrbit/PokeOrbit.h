
// PokeOrbit.h : main header file for the PokeOrbit application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CPokeOrbitApp:
// //

/**
 * See PokeOrbit.cpp for the implementation of this class
 */
class CPokeOrbitApp : public CWinApp
{
public:
	CPokeOrbitApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()

private:
	/// The startup input
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	/// token to the gdiplus members
	ULONG_PTR gdiplusToken;
};

extern CPokeOrbitApp theApp;
