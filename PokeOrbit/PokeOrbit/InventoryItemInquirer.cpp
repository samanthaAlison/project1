/**
 * \file InventoryItemInquirer.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "InventoryItemInquirer.h"


/**
 * Constructor. Gives the inquirer a type to look for
 * \param type The type of item to look for
 */
CInventoryItemInquirer::CInventoryItemInquirer(CItemType::Type type)
{
	mItemType = type;
}


/**
 * Destructor.
 */
CInventoryItemInquirer::~CInventoryItemInquirer()
{
}


/**
 * Visits an inventory item to see what type it is.
 * \param item Item to visit
 */
void CInventoryItemInquirer::VisitInventoryItem(CInventoryItem * item)
{
	if (item->GetItemType() == mItemType)
	{
		mFoundCount++;
	}
}

