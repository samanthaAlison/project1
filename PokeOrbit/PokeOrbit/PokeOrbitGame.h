/**
 * \file PokeOrbitGame.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * \author Keerthana Kolisetty
 *
 * Describes a main/system class for the the game. Is also the root drawable game component.
 */


#pragma once
#include "DrawableGameComponent.h"
#include "ItemType.h"
class CInventory;
class CPlayingCircle;
class CPokemon;
class CScoreboard;


 /**
 * The main/system class for the the game
 */
class CPokeOrbitGame :	public CDrawableGameComponent
{
public:
	
	/* Default constructor used to initialize the game before 
	 * the window is fully intialized. */
	CPokeOrbitGame() : CDrawableGameComponent(0.0, 0.0) {}
	CPokeOrbitGame(double w, double h);
	virtual ~CPokeOrbitGame();
	void Initialize();
	void ThrowPokeball(double x, double y);

	/**
	* Draws the frame of the game window.
	* \param graphics The graphics to draw.
	* \param w The width of the frame.
	* \param h The height of the frame.
	*/
	void DrawFrame(Gdiplus::Graphics * graphics, int w, int h);

	bool HitTest(int x, int y);
	virtual void PokemonCaught(CPokemon * pokemon) override;
	virtual void AddPokeballs(int count) override;
	// Test to see if click happened inside of the circle
	bool ClickTest(int x, int y);
	
private:

	/// The game's playing circle.
	std::shared_ptr<CPlayingCircle> mCircle;

	/// The game's inventory of pokeballs.
	std::shared_ptr<CInventory> mInventory;
	
	/// The game's scoreboard of how many pokemon were caught.
	std::shared_ptr<CScoreboard> mScoreboard;

	/// The scale of the game depending on the window size.
	float mScale;
};

