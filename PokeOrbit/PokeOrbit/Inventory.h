/**
 * \file Inventory.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes a menu that displays the player's inventory.
 *
 * Holds inventory items that can be dynamically added and removed.
 */


#pragma once
#include "DrawableGameComponentChild.h"
#include "InventoryItem.h"
#include "ItemPokeball.h"
#include "ComponentFactory.h"
#include "InventoryItemInquirer.h"
#include <vector>


/**
 * Class that display's the player's inventory.
 */
class CInventory : public CDrawableGameComponentChild
{
public:
	// Copy constructor deleted.
	CInventory(const CInventory &) = delete;

	// Constructor
	CInventory(CDrawableGameComponent * parent, double x, double y);
	// Destructor
	virtual ~CInventory();
	
	// Adds inventory items to the inventory.
	void AddItem(CItemType::Type item, int count);

	// Removes items of a certain type from the inventory. 
	bool RemoveItem(CItemType::Type item, int count);

	// Counts the amount of a certain item this inventory has.
	int GetItemCount(CItemType::Type item);
};

