/**
 * \file Emitter.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class that controls emitting for pokemon and pokestops.
 */


#pragma once
#include "NormalDistribution.h"

class CPlayingCircle;


/**
 * Class that controls emitting for pokemon and pokestops.
 */
class CEmitter 
{
public:
	// Default constructor deleted
	CEmitter()=delete;

	// Emitter constructor
	CEmitter(CPlayingCircle *mCircle);

	// Emitter destructor
	virtual ~CEmitter();

	// Update function for emitting game components
	void Update(double elapsed);
	
private:
	///Keeps track of game time in seconds.
	double mTimer;

	///Time since last pokemon emission
	double mLastPokemonEmission;
	/// Time since last pokestop emission;
	double mLastPokestopEmission;

	// Emits a pokemon
	void AddPokemon();
	// Emits a pokestop
	void AddPokestop();

	/// The circle where the game is played
	CPlayingCircle *mCircle;
	
	/// Normal distribution for pokemon
	CNormalDistribution * mPokemonNormal;
	/// Normal distribution for pokestops
	CNormalDistribution * mPokestopNormal;
};

