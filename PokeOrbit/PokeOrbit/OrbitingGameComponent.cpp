/**
 * \file OrbitingGameComponent.cpp
 *
 * \author Samantha Oldenburg
 * \author Keerthana Kolisetty
 */


#include "stdafx.h"
#include "OrbitingGameComponent.h"
#include <random>


/**
 * Constructor. Assigns the parent of this component, gives it a position, and the filename of its sprite.
 * \param parent Parent of this component.
 * \param x X coordinate of the component.
 * \param y Y coordinate of the component.
 * \param filename Filename of the component's sprite
 */
COrbitingGameComponent::COrbitingGameComponent(CDrawableGameComponent * parent, double x, double y, const std::wstring &filename) : CSpritedGameComponent(parent, x, y, filename)
{
}


/**
 * Destructor.
 */
COrbitingGameComponent::~COrbitingGameComponent()
{
}


/**
 * Updates the component relative to the time elapsed from the last frame.
 * \param elapsed Time elapsed since last frame.
 */
void COrbitingGameComponent::Update(double elapsed)
{
	mRadius += mRadiusSpeed * elapsed; 
	mTheta += mThetaSpeed * elapsed;
	TransformToCartesian();
	CDrawableGameComponent::Update(elapsed);
}


/**
 * Transforms the components coordinates from theta and radius
 * to x and y, so that it can be drawn with the same methods as 
 * all drawable game components.
 */
void COrbitingGameComponent::TransformToCartesian()
{
	SetX(mRadius * cos(mTheta));
	SetY(mRadius * sin(mTheta));
}


/**
* Generates a random radius
* \return int random radius
*/
int COrbitingGameComponent::GetRandomRadius()
{
	/// Random device for Orbiting Game component
	std::random_device rd;

	/// Random generator for Orbiting Game component
	std::mt19937 rng(rd());

	/// Range for random ints
	std::uniform_int_distribution<int> distr(100, 315);

	return distr(rng);
}


/**
* Generates a random speed
* \return int random speed
*/
double COrbitingGameComponent::GetSpeed()
{
	/// Random device for Orbiting Game component
	std::random_device rd;

	/// Random generator for Orbiting Game component
	std::mt19937 rng(rd());

	/// Range for random doubles
	std::uniform_real_distribution<double> rad(.5, 2.5);

	return rad(rng);
}