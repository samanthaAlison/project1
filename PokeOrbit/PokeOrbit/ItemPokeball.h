/**
 * \file ItemPokeball.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class that describes pokeball items in the inventory.
 */


#pragma once
#include "InventoryItem.h"
#include "ItemType.h"


/**
 * Pokeball item class derived from the inventory item class.
 */
class CItemPokeball :
	public CInventoryItem
{
public:
	// Copy constructor deleted
	CItemPokeball(const CItemPokeball &) = delete;

	// Pokeball item constructor
	CItemPokeball(CDrawableGameComponent * parent, double x, double y);
	// Pokeball item destructor
	virtual ~CItemPokeball();
};

