/**
 * \file InventoryItem.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "InventoryItem.h"


/**
 * Constructor. Assigns the parent of this inventory item, gives it a position, 
 * the filename of its sprite, and the type of inventory item it is.
 * \param parent Parent of this component.
 * \param x X coordinate of the component.
 * \param y Y coordinate of the component.
 * \param filename Filename of the component's sprite
 * \param item The type of item it is.
 */
CInventoryItem::CInventoryItem(CDrawableGameComponent * parent, double x, double y, const std::wstring &filename, CItemType::Type item) : CSpritedGameComponent(parent, x, y, filename)
{
	mItemType = item;
}


/**
 * Destructor.
 */
CInventoryItem::~CInventoryItem()
{
}

