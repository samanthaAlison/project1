/**
* \file PokeballInstance.cpp
*
* \author Stefani Taskas
* \author Samantha Oldenburg
*/


#include "stdafx.h"
#include "PokeballInstance.h"
#include "PokeOrbitGame.h"

using namespace std;
using namespace Gdiplus;


/// Image of the pokeball
const std::wstring PokeballSprite(L"images/pokeball.png");


/**
* Pokeball Constructor
* \param parent The parent of this component
* \param theta The theta value of the pokeball.
* \param radiusSpeed The speed at which the pokeball will leave the circle.
*/
CPokeballInstance::CPokeballInstance(CDrawableGameComponent * parent, double theta, double radiusSpeed) : COrbitingGameComponent(parent, 0.0, 0.0, PokeballSprite)
{
	SetRadiusSpeed(radiusSpeed);
	SetThetaSpeed(0.0);
	SetTheta(theta);
	SetRadius(0.0);
}


/**
* Destructor
*/
CPokeballInstance::~CPokeballInstance()
{
}


/**
 * Update the location of the pokeball and check if a pokemon was caught.
 * \param elapsed The time passed since the class call.
 */
void CPokeballInstance::Update(double elapsed) {
	COrbitingGameComponent::Update(elapsed);

	// If pokeball is out of bounds, delete it
	if (OutOfBounds())
	{
		this->Remove();
	}
	else if (GetParent()->Caught(this))
	{
		// Moving pokeball caught a pokemon
		// Delete the pokeball
		this->Remove();
	}
}


/**
 * Draws the pokeball.
 * \param graphics The pokeball image graphics
 */
void CPokeballInstance::Draw(Gdiplus::Graphics * graphics)
{
	COrbitingGameComponent::Draw(graphics);
}