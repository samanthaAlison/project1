/**
 * \file NormalDistribution.h
 *
 * \author Samantha Oldenburg
 *
 * Describes class that creates a somewhat normally distributed
 * probability of an object spawning.
 * 
 * The curve created by this function can be described by the following formula:
 * P(x) = C / (SD * SQRT(2 * PI)) * e^(-(x - M)^2/(2 * SD^2))
 * Where C is the calculated coefficient and SD is the standard deviation,
 * and M is the mean. SD and C are calculated such that P(mMinTime) = MinProbability 
 * and P(M) = MaxProbability. 
 */


#pragma once

/**
 * Creates a somewhat normally distributed
 * probability of an object spawning.
 */
class CNormalDistribution
{
public:
	CNormalDistribution() = delete;
	CNormalDistribution(double minTime, double maxTime);
	~CNormalDistribution();
	bool WillEmissionOccur(double time);
private:

	/// Minimum time since previous emission before an emission can take place.
	double mMinTime;
	/// Maximum time since previous emission before an emission MUST take place.
	/// If the time exceeds this number, WillEmissionOccur will return true
	double mMaxTime;

	/// Mean of the distribution. Will be the center point between min and max 
	/// times.
	double mMean;
	
	/// This section of the formula: { C / (SD * SQRT(2 * PI)) }. Is stored 
	/// because it only needs to be calculated once.
	double mCoefficientA;

	/// This section of the formula: { (2 * SD^2) }. Is stored 
	/// because it only needs to be calculated once.
	double mCoefficientB;

	
};