/**
 * \file DrawableGameComponentVisitor.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "DrawableGameComponentVisitor.h"


/**
 * Constructor.
 */
CDrawableGameComponentVisitor::CDrawableGameComponentVisitor()
{
}


/**
 * Destructor.
 */
CDrawableGameComponentVisitor::~CDrawableGameComponentVisitor()
{
}
