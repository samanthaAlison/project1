/**
* \file PokemonType.cpp
*
* \author Keerthana Kolisetty
* \author Samantha Oldenburg
*/


#include "stdafx.h"
#include "PokemonType.h"

/// wstring for bulbasuar
const std::wstring CPokemonType::BulbasaurSprite(L"images/bulbasaur.png");

/// wstring for charmander
const std::wstring CPokemonType::CharmanderSprite(L"images/charmander.png");

/// wstring for pikachu
const std::wstring CPokemonType::PikachuSprite(L"images/pikachu.png");


/**
 * Finds the type of the pokemon
 * \param type, type of pokeone
 * \returns wstring the sprite of wanted pokemon
 */
const std::wstring & CPokemonType::GetFile(Type type)
{
	switch (type)
	{
	case (Bulbasaur):
		return BulbasaurSprite;
	case (Charmander):
		return CharmanderSprite;
	case (Pikachu):
		return PikachuSprite;
	}
}
