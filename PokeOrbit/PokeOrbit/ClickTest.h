/**
 * \file ClickTest.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Describes a class that checks if a pokestop has been clicked.
 */


#pragma once
#include "DrawableGameComponentVisitor.h"
class CPokestop;


/**
 * Class that checks if a pokestop has been clicked.
 */
class CClickTest :
	public CDrawableGameComponentVisitor
{
public:
	// Click test constructor
	CClickTest(int x, int y);
	// Click test destructor
	virtual ~CClickTest();

    /**
	 * Gets if this object has found a pokestop
	 * \returns True if the this object has found a pokestop
	 * at the assign x,y position
	 */
	bool FoundOne() { return mFoundOne; }

	// Visits a pokestop to see if it has been clicked.
	virtual void VisitPokestop(CPokestop * pokestop);

private:
	/// X value of click
	int mX;
	/// Y value of click
	int mY;

	/// True if there is a pokestop at the x,y position
	bool mFoundOne;
};


