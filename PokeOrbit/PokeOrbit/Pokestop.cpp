/**
* \file Pokestop.cpp
*
* \author Keerthana Kolisetty
* \author Stefani Taskas
* \author Samantha Oldenburg
*/


#include "stdafx.h"
#include "Pokestop.h"
#include <random>
using namespace Gdiplus;
using namespace std;


/// image filename of the used pokestopsprite
const wstring UsedPokestopSprite(L"images/pokestop-used.png");

/// image filename of the pokestop sprite
const wstring PokestopSprite(L"images/pokestop.png");

/// Value of pi
const double Pi = 3.1415926535897;




/**
 * Constructor. Gives the pokestop a pair of x, y coordinates and assigns
 * its parents
 * \param parent Parent of this pokestop 
 * \param x X coordinate to place this pokestop at
 * \param y Y coordinate to place thic pokestop at
 */
CPokestop::CPokestop(CDrawableGameComponent * parent, double x, double y) : COrbitingGameComponent(parent, x, y, PokestopSprite)
{
	/// Random device for pokestops
	std::random_device rdStop;
	/// Random generator for pokestops
	std::mt19937 rngStop(rdStop());
	/// Range for random doubles
	std::uniform_real_distribution<double> radStop(.1, .7);


	mUsedSprite = unique_ptr<Bitmap>(Bitmap::FromFile(UsedPokestopSprite.c_str()));
	if (mUsedSprite->GetLastStatus() != Ok)
	{
		wstring msg(L"Failed to open ");
		msg += UsedPokestopSprite;
		AfxMessageBox(msg.c_str());
	}

	SetRadius(GetRandomRadius());
	SetTheta(radStop(rngStop));
	SetThetaSpeed(radStop(rngStop));
	SetRadiusSpeed(0.0);
	mTimeAlive = 0.0;
	mTimeUsed = 0.0;
}


/**
 * Destructor.
 */
CPokestop::~CPokestop()
{
}


/**
 * Call when pokestop is clicked. 
 */
void CPokestop::PokestopClicked()
{
	// Don't add pokeballs if the pokestop 
	// hasn't been used yet
	if (!mClicked)
	{
		AddPokeballs(3);
		mClicked = true;
	}
}


/**
 * Draws this component. Overrides parent's draw function.
 * \param graphics Pointer to Graphics object 
 */
void CPokestop::Draw(Gdiplus::Graphics * graphics)
{
	// If the pokestop has been used, display the used 
	// pokestop sprite
	if (mClicked) {
		int XLocation =(int) this->GetX();
		int YLocation = (int) this->GetY();
		graphics->DrawImage(mUsedSprite.get(), (float)(GetX() - GetWidth() * GetScale()), float(GetY() - GetHeight() * GetScale()),
			(float)CDrawableGameComponent::GetWidth() * (float)GetScale(), (float)CDrawableGameComponent::GetHeight() * (float)GetScale());
	}
	// Use the default sprite.
	else
	{
		CSpritedGameComponent::Draw(graphics);
	}
}


/**
 * Updates the pokestop.  
 * \param elapsed Time since last frame was drawn 
 */
void CPokestop::Update(double elapsed)
{
	COrbitingGameComponent::Update(elapsed);

	mTimeAlive += elapsed;

	// If it's been 60 seconds since spawn time delete
	// this pokestop
	if (mTimeAlive >= 60.00)
	{
		Remove();
	}
	if (mClicked)
	{
		mTimeUsed += elapsed;
	}

	// If it's been 15 seconds since pokestop has been 
	// used, pokestop can be used again.
	if (mTimeUsed >= 15.0)
	{
		mClicked = false;
		mTimeUsed = 0.0;
	}
}

