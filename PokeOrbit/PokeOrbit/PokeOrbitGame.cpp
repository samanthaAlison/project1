/**
 * \file PokeOrbitGame.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * \author Keerthana Kolisetty
 */


#include "stdafx.h"
#include "PokeOrbitGame.h"
#include "PlayingCircle.h"
#include "ComponentFactory.h"
#include "Pokestop.h"
#include "Pokemon.h"
#include "Pikachu.h"
#include "Bulbasaur.h"
#include "Charmander.h"
#include "InventoryItem.h"
#include "Inventory.h"
#include "PokeballInstance.h"
#include "Scoreboard.h"


/// Y of Scoreboard
const double ScoreboardY(30.0);
/// X of Scoreboard
const double ScoreboardX(820.0);
/// Y of Inventory
const double InventoryY(30.0);
/// X of Inventory
const double InventoryX(40.0);


/**
 * Constructor.
 * \param w Width of the game.
 * \param h Height of the game.
 */
CPokeOrbitGame::CPokeOrbitGame(double w, double h) : CDrawableGameComponent(0.0, 0.0)
{
	SetWidth(w);
	SetHeight(h);
}


/**
 * Destructor.
 */
CPokeOrbitGame::~CPokeOrbitGame()
{
}


/**
 * Initializes the game object post-construction.
 */
void CPokeOrbitGame::Initialize()
{
	CComponentFactory factory;
	mCircle = factory.CreatePlayingCircle(this, GetWidth() / 2.0, GetHeight() / 2.0);
	AddChild(mCircle);
	mCircle->Initialize();

	mInventory = std::make_shared<CInventory>(this, InventoryX, InventoryY);
	AddChild(mInventory);
	
	mScoreboard = std::make_shared<CScoreboard>(this, ScoreboardX, ScoreboardY);
	AddChild(mScoreboard);

	mInventory->AddPokeballs(3);
}


/**
 * Checks if the player has pokeballs, and if so, tell the 
 * playing circle to throw one.
 * \param x X coordinate of the click.
 * \param y Y coordinate of the click.
 */
void CPokeOrbitGame::ThrowPokeball(double x, double y)
{
	if (mInventory->GetItemCount(CItemType::Pokeball) > 0)
	{
		
		mCircle->ThrowPokeball(x / mScale - mCircle->GetX(), y / mScale - mCircle->GetY() );
		mInventory->RemoveItem(CItemType::Pokeball, 1);
	}
}


/* Draws the current frame of the game
 * \param graphics The pointer to the graphics renderer.
 * \param w Width of frame
 * \param h Height of frame
 */
void CPokeOrbitGame::DrawFrame(Gdiplus::Graphics * graphics, int w, int h)
{
	float scaleX = float(w) / float(GetWidth());
	float scaleY = float(h) / float(GetHeight());
	mScale = min(scaleX, scaleY);
	/*float xOffset = width / 2.0f;
	float yOffset = height / 2.0f;

	graphics->TranslateTransform(xOffset, yOffset);
*/
	graphics->ScaleTransform(mScale, mScale);
	Draw(graphics); 
}


/**
 * Transforms the click from virtual pixels and tests to see if
 * any object in the Playing Circle has been clicked
 * \param x X coordinate of click in Virtual pixels
 * \param y Y coordinate of click in Virtual pixels
 * \returns true if an object was clicked
 */
bool CPokeOrbitGame::HitTest(int x, int y)
{
	float screenX = x / mScale;
	float screenY = y / mScale;
	return mCircle->HitTest((int) screenX, (int) screenY);
}


/**
 * Code called when a pokeball hits a pokemon. Adds a point 
 * to the scoreboard.
 * \param pokemon Pointer to the pokemon object we caught.
 */
void CPokeOrbitGame::PokemonCaught(CPokemon * pokemon)
{
	mScoreboard->AddPoint(pokemon->GetType());
}


/**
 * Adds pokeballs to the inventory
 * \param count The amount of pokeballs to add
 */
void CPokeOrbitGame::AddPokeballs(int count)
{
	mInventory->AddItem(CItemType::Pokeball, count);
}


/**
* Checks if the click was inside of the circle
* \param x X position of the click
* \param y Y position of the click
* \returns true if clicked in the circle, false if clicked outside the circle
*/
bool CPokeOrbitGame::ClickTest(int x, int y)
{

	float screenX = x / mScale;
	float screenY = y / mScale;
	// The distance from the center of the circle to the click
	double radiusClick = sqrt(pow((screenX - GetWidth()/2.0f), 2) + pow((screenY - GetHeight() / 2.0f), 2));

	// Test to see if x, y are in the circle
	if (radiusClick > mCircle->GetRadius())
	{
		// We are outside the circle
		return false;
	}
	// The click was in the circle
	return true;
}