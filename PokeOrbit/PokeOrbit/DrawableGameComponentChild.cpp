/**
 * \file DrawableGameComponentChild.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 */


#include "stdafx.h"
#include "DrawableGameComponentChild.h"
#include "ClickTest.h"


/**
 * Constructor. Adds this component as a child of its parent.
 * \param parent Parent component of this component.
 * \param x X coordinate of this component.
 * \param y Y coordinate of this component.
 */
CDrawableGameComponentChild::CDrawableGameComponentChild(CDrawableGameComponent * parent, double x, double y) : CDrawableGameComponent(x, y)
{
	mParent = parent;	
}


/**
 * Destructor.
 */
CDrawableGameComponentChild::~CDrawableGameComponentChild()
{

}


/**
 * Gets the absolute Y coordinates of this component. 
 * \returns The absolute Y coordinate (not relative to immediate parent).
 */
double CDrawableGameComponentChild::GetY()
{
	return CDrawableGameComponent::GetY() + mParent->GetY();

}


/**
* Gets the absolute X coordinates of this component.
* \returns The absolute X coordinate (not relative to immediate parent).
*/
double CDrawableGameComponentChild::GetX()
{
	return CDrawableGameComponent::GetX() + mParent->GetX();

}


/**
 * Checks to see if a pokestop was clicked.
 * \param x The x coordinate of the click.
 * \param y The y coordinate of the click.
 * \returns boolean true or false depending on if a pokestop was clicked on.
 */
bool CDrawableGameComponentChild::HitTest(int x, int y)
{
	CClickTest clickTest(x, y);
	for (std::vector<std::shared_ptr<CDrawableGameComponentChild > >::iterator iter = ChildrenBegin(); iter != ChildrenEnd(); ++iter)
	{
		(*iter)->Accept(&clickTest);
		if (clickTest.FoundOne())
		{
			// A pokestop was clicked
			return true;
		}

	}
	// None of the pokestops were clicked
	return false;
}
