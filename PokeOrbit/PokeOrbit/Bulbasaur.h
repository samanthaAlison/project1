/**
* \file Bulbasaur.h
*
* \author Keerthana Kolisetty
* \author Stefani Taskas
*
* Derived class of pokemon for the bulbasaur pokemon
*/


#pragma once
#include "Pokemon.h"


/**
* Bulbasaur class whose base class is Pokemon
*/
class CBulbasaur :
	public CPokemon
{
public:
	// The bulbasaur constructor
	CBulbasaur(CDrawableGameComponent * parent, double x, double y);
	// The bulbasaur destructor
	virtual ~CBulbasaur();
};

