/**
 * \file SpritedGameComponent.h
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 *
 * Class that describes a type of DrawableGameComponent that has a 2D sprite that it draws.
 */

#pragma once
#include "DrawableGameComponentChild.h"
#include <string>


/**
 * DrawableGameComponent that has a 2D sprite that it draws.
 */
class CSpritedGameComponent : public CDrawableGameComponentChild
{
public:
	CSpritedGameComponent(const CSpritedGameComponent &) = delete;
	CSpritedGameComponent(CDrawableGameComponent * parent, double x, double y, const std::wstring &filename);
	virtual ~CSpritedGameComponent();

	/** Overrides base GetWidth function to apply scale to width
	 * \returns the width of this component multiplied by its scale 
	 */
	virtual double GetWidth() { return CDrawableGameComponent::GetWidth() * GetScale(); }
	
	/** Overrides base GetHeight function to apply scale to height
	* \returns the height of this component multiplied by its scale 
	*/
	virtual double GetHeight() { return CDrawableGameComponent::GetHeight() * GetScale(); }

	// Checks if the pokeball is over a pokemon
	virtual bool IsAtCoordinates(int x, int y) override;

	
protected:
	virtual void Draw(Gdiplus::Graphics *graphics) override;

	/** Gets the scale the sprite is set to.
	 * \returns the scale of the sprite. */
	double GetScale() { return mScale; }
private:
	
	/// The sprite that will be drawn in this component.
	std::unique_ptr<Gdiplus::Bitmap> mSprite;

	/// Scale to set sprite to. 1.0 is full size
	double mScale = 0.5;
};

