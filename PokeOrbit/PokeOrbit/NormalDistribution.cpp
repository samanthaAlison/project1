/**
* \file NormalDistribution.cpp
*
* \author Samantha Oldenburg
* \author Keerthana Kolisetty
*/


#include "stdafx.h"
#include "NormalDistribution.h"


/// Minimum probability an object will be spawned.
const double MinProbability = 0.05;

/// Maximum probability an object will be spawned.
const double MaxProbability = 0.25;

/// Value of pi
const double Pi = 3.1415926535897;


/**
* Constructor
* \param minTime the minimum time wanted
* \param maxTime the maximum time wanted
*/
CNormalDistribution::CNormalDistribution(double minTime, double maxTime)
{
	srand((unsigned int)time(nullptr));

	mMinTime = minTime;
	mMaxTime = maxTime;

	mMean = mMinTime + (mMaxTime - mMinTime) / 2;
	
	// Calculated value of the standard devation in 
	// the formula in the class definition.
	double sD = sqrt(-1 * (pow((mMinTime - mMean), 2.0) /
		(2 * log(MinProbability / MaxProbability) ) ) );
	// Calculated value of the C coefficient in the formula in
	// the class definition.
	double coefficient = MaxProbability * sD * sqrt(2 * Pi);

	mCoefficientA = coefficient / (sD * sqrt(2 * Pi));
	mCoefficientB = 2 * pow(sD, 2.0);
}


/**
* Destructor
*/
CNormalDistribution::~CNormalDistribution()
{
}


/**
* Determines if an emission will occur or not
* \param time the elapsed time
* \returns bool true or false if emission should occur or not
*/
bool CNormalDistribution::WillEmissionOccur(double time)
{
	if (time < mMinTime)
	{
		return false;
	}
	else if (time >= mMaxTime)
	{
		return true;
	}
	double pChance = mCoefficientA * exp(-1 * pow( (time - mMean), 2.0) / mCoefficientB);
	double pRoll = ((double)rand() / (RAND_MAX));
	return (pChance >= pRoll);
}

