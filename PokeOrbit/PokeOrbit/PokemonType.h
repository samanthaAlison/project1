/**
* \file PokemonType.h
*
* \author Samantha Oldenburg
* Class that describes each of the pokemon types
*/

#pragma once
#include <string> 

/**
 * Static class used to describe a pokemon's types
 */
class CPokemonType
{ 
public:
	/// Type of pokemon, Bulbasaur, Pikachu, Charmander
	enum Type { Bulbasaur, Pikachu, Charmander };
	static const std::wstring& GetFile(Type type);
private:
	///String to access the picture of BulbasaurSprite
	const static std::wstring BulbasaurSprite;

	///String to access the picture of Charmandersprite
	const static std::wstring CharmanderSprite; 

	///String to access the picture of PikachuSprite
	const static std::wstring PikachuSprite;

};
