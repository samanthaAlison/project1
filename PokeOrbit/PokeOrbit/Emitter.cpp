/**
 * \file Emitter.cpp
 *
 * \author Samantha Oldenburg
 * \author Stefani Taskas
 * \author Keerthana Kolisetty
 */


#include "stdafx.h"
#include "Emitter.h"
#include "PlayingCircle.h"
#include "ComponentFactory.h"
#include "Pikachu.h"
#include "Bulbasaur.h"
#include "Charmander.h"
#include "Pokestop.h"


/// Minimum time before new pokemon can spawn.
const double MinPokemonTime = 3.0;
/// Maximum time before new pokemon MUST spawn.
const double MaxPokemonTime = 10.0;

/// Minimum time before new pokestop can spawn.
const double MinPokestopTime = 15.0;
/// Maximum time before new pokestop MUST spawn.
const double MaxPokestopTime = 50.0;


/**
 * Constructor.
 * \param circle The playing circle for the game.
 */
CEmitter::CEmitter(CPlayingCircle * circle)
{
	mCircle = circle;
	mTimer = 0.0;
	mPokestopNormal = new CNormalDistribution(MinPokestopTime, MaxPokestopTime);
	mPokemonNormal = new CNormalDistribution(MinPokemonTime, MaxPokemonTime);
	
	bool escape = false;
	int a = 0;
	while (!escape)
	{
		a++;
		escape = mPokemonNormal->WillEmissionOccur(4.1);
	}
}


/**
 * Destructor.
 */
CEmitter::~CEmitter()
{
	delete mPokemonNormal;
	delete mPokestopNormal;
}


/**
 * Controls the emissions for the pokemon and pokestops.
 * \param elapsed The amount of time passed since the last frame drawn.
 */
void CEmitter::Update(double elapsed)
{
	mTimer += elapsed;
	if (mPokemonNormal->WillEmissionOccur(mTimer - mLastPokemonEmission))
	{
		AddPokemon();
	}
	if (mPokestopNormal->WillEmissionOccur(mTimer - mLastPokestopEmission))
	{
		AddPokestop();
	}
}


/**
 * Adds a random pokemon to the game.
 */
void CEmitter::AddPokemon()
{
	mLastPokemonEmission = mTimer;
	CComponentFactory factory;

	int RandNum = (rand() % 3 + 1);
	if (RandNum == 1) {
		std::shared_ptr<CPikachu>  pikachu = CComponentFactory::CreatePikachu(mCircle, 0.0, 0.0);
		mCircle->AddChild(pikachu);
	}
	if (RandNum == 2) {
		std::shared_ptr<CBulbasaur> bulbasaur = CComponentFactory::CreateBulbasaur(mCircle, 0.0, 0.0);
		mCircle->AddChild(bulbasaur);
	}
	if (RandNum == 3) {
		std::shared_ptr<CCharmander> charmander = CComponentFactory::CreateCharmander(mCircle, 0.0, 0.0);
		mCircle->AddChild(charmander);
	}
}


/**
 * Adds a pokestop to the game.
 */
void CEmitter::AddPokestop()
{
	CComponentFactory factory;

	mLastPokestopEmission = mTimer;
	std::shared_ptr<CPokestop> pokestop = factory.CreatePokestop(mCircle, 0.0, 0.0);
	mCircle->AddChild(pokestop);
}
