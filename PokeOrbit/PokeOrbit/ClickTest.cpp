/**
 * \file ClickTest.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "ClickTest.h"
#include "Pokestop.h"


/**
 * Constructor. Assigns x, y coordinates to look for pokestops
 * on.
 * \param x X coordinate of click
 * \param y Y coordinate of click
 */
CClickTest::CClickTest(int x, int y)
{
	mX = x;
	mY = y;
	mFoundOne = false;  
}


/**
 * Destructor. 
 */
CClickTest::~CClickTest()
{
}


/**
 * Visits a pokestop to see if it has been clicked.
 * \param pokestop Pokestop that is visited
 */
void CClickTest::VisitPokestop(CPokestop * pokestop)
{
	// Checks if a pokestop is located at the click
	if (pokestop->IsAtCoordinates(mX, mY))
	{
		pokestop->PokestopClicked();
		mFoundOne = true;
	}
}