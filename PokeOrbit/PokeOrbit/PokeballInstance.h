/**
* \file PokeballInstance.h
*
* \author Stefani Taskas
* \author Samantha Oldenburg
* \author Keerthana Kolisetty
* Class for the moving pokeball used to catch pokemon
*/


#pragma once
#include "OrbitingGameComponent.h"


/**
* Pokeball class whose base class is OrbitingGameComponent
*/
class CPokeballInstance :
	public COrbitingGameComponent
{
public:
	/// Default constructor (disabled)
	CPokeballInstance() = delete;

	/// Copy constructor (disabled)
	CPokeballInstance(const CPokeballInstance &) = delete;

	CPokeballInstance(CDrawableGameComponent * parent, double theta, double radiusSpeed);
	virtual ~CPokeballInstance();

	// Handles updates for the movement of the pokeball
	virtual void Update(double elapsed);

	/**
	* Tells us if the pokeball has left the radius of the circle
	* \returns true if out of the circle, false otherwise
	*/
	virtual bool OutOfBounds() override { return GetRadius() > 350.0; }
	
	// Draws the pokeball
	virtual void Draw(Gdiplus::Graphics *graphics);
};
