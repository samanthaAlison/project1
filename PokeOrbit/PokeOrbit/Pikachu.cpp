/**
* \file Pikachu.cpp
*
* \author Keerthana Kolisetty
* \author Stefani Taskas
*/


#include "stdafx.h"
#include "Pikachu.h"

#include <random>


/// Maximum speed of a pikachu in radians per second
const double PikachuMaxSpeed(2.5);
/// Minimum speed of a pikachu in radians per second
const double PikachuMinSpeed(2.0);


/**
 * Constructor
 * \param parent Parent of this pikachu 
 * \param x The x coordinate to place this pikachu at
 * \param y The y coordinate to place thic pikachu at
 */
CPikachu::CPikachu(CDrawableGameComponent * parent, double x, double y): CPokemon(parent, x, y, CPokemonType::Pikachu)
{
	/// Random device 
	std::random_device rdPika;
	/// Random generator 	
	std::mt19937 rngPika(rdPika());
	/// Range for random doubles
	std::uniform_real_distribution<double> radPika(PikachuMinSpeed, PikachuMaxSpeed);

	SetRadius(GetRandomRadius());
	SetTheta(GetSpeed());
	SetThetaSpeed(radPika(rngPika));
	SetRadiusSpeed(0.0);
}


/**
 * Destructor
 */
CPikachu::~CPikachu()
{
}
