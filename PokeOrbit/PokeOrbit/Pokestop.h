/**
 * \file Pokestop.h
 *
 * \author Samantha Oldenburg
 * \author Keerthana Kolisetty
 * \author Stefani Taskas
 *
 * Class that describes a Pokestop object that spawns in the playing circle.
 * When the user clicks on a Pokestop, they get items. 
 */


#pragma once
#include "OrbitingGameComponent.h"
#include <string>


/**
 * Pokestop object used in the game.
 */
class CPokestop : public COrbitingGameComponent
{
public:
	CPokestop(CDrawableGameComponent * parent, double x, double y);
	virtual ~CPokestop();
	void PokestopClicked();

	/**
	* Accepts a visitor to this pokestop
	* \param visitor The game component visitor
	*/
	virtual void Accept(CDrawableGameComponentVisitor * visitor) { visitor->VisitPokestop(this); }
	
	// Draw the graphics for the pokestop
	virtual void Draw(Gdiplus::Graphics *graphics) override;

protected:
	virtual void Update(double elapsed);

private:
	/// Sprite to show when the Pokestop is not ready to be clicked again.
	std::unique_ptr<Gdiplus::Bitmap> mUsedSprite;
	/// Is true when the pokestop has been used and 
	/// can't be clicked again.
	boolean mClicked = false;
	
	/// Time since the pokestop has changed to used state 
	double mTimeUsed = 0.0;
	
	/// Time since the pokestop has spawned.
	double mTimeAlive = 0.0;
};

